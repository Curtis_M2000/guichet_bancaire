#include <string>
#include <iostream>
#include <fstream>
using namespace std;

struct Client {
	string numero;
	string nom;
	string nip;
	float solde;
	Client *suivant;
};

struct Liste {
	int nbClient;
    Client *premier;
    Client *dernier;
};

void afficheTitre() {
	cout << "\t\t BANQUE ROYAL" << endl;
	cout << "\t\t ------------" << endl;
	cout << "\t Guichet Automatique Bancaire" << endl;
	cout << "\t ----------------------------" << endl;
}

float convertirEnFloat(string val) {
	if (!stof(val)) {
		return 0;
	}
	else {
		return stof(val);
	}
}

void inverser(Liste *uneliste, Client *unClient) {
	if (uneliste->nbClient == 0) {
		uneliste->premier = unClient;
		uneliste->dernier = unClient;
		uneliste->nbClient = uneliste->nbClient + 1;
	}
	else {
		uneliste->dernier->suivant = unClient;
		uneliste->dernier = unClient;
		uneliste->nbClient = uneliste->nbClient + 1;
	}
}

void recuperCompteDuFichier(Liste *uneliste, Client *unClient) {
	int i=0;
	string text, nocompte, nip;
	ifstream monFichier;
	
	do {
		monFichier.open("fichierGAB.txt");
		cout << "Entrez votre numero de compte : ";
		getline(cin, nocompte);

		while (monFichier) {
			getline(monFichier, text);

			if ((i == 0 || i % 4 == 0) && text == nocompte && unClient->numero == "") { //1er condition verifie juste les numero dans le fichier. 2eme verifie si se numero est equivalant a celui entree. 3eme verifie si notre client etait deja vide
				unClient->numero = text;
			}
			else if (unClient->numero != "" && unClient->nom == "") {
				unClient->nom = text;
				cout << "\n\tBienvenue " << unClient->nom << endl;	
			}
			else if (unClient->nom != "" && unClient->nip == "") {
				do {
					cout << "Entrez votre nip : ";
					getline(cin, nip);
				} while (nip != text);
				unClient->nip = text;
			}
			else if (unClient->nip != "" && unClient->solde == NULL) {
				
				unClient->solde = convertirEnFloat(text);
			}
		}

		if (unClient->numero == "") {
			cout << "Client non-trouve" << endl;
		}
		else {
			inverser(uneliste, unClient);
		}
		monFichier.close();
	} while (unClient->solde == NULL);
}

void consulter(Liste *uneliste) {
	cout << "\nLes information du compte" << endl;
	for (Client *courant = uneliste->premier; courant != NULL; courant = courant->suivant) {
		cout << "\tNumero : " << courant->numero << endl;
		cout << "\tClient : " << courant->nom << endl;
		cout << "\tNip : " << courant->nip << endl;
		cout.precision(10);
		cout << "\tSolde $ : " << courant->solde << "\n" <<endl;
	}
}

void retirer(Client *unClient) {
	string val; float retrait;
	do {
		cout << "\nEntrez le montant a retirer : ";
		getline(cin, val);
		retrait = convertirEnFloat(val);
	} while (retrait < 20 || retrait > 500 || retrait > unClient->solde || (int)retrait % 20 != 0);
	unClient->solde = unClient->solde - retrait;
	cout << "--- La transaction a reussi ---\n" << endl;
}

void deposer(Client *unClient) {
	string val; float depot;
	do {
		cout << "\nEntrez le montant a deposer : ";
		getline(cin, val);
		depot = convertirEnFloat(val);
	} while (depot < 2 || depot > 20000);
	unClient->solde = unClient->solde + depot;
	cout << "--- La transaction a reussi ---\n" << endl;
}

void choisirTransaction(Liste *uneliste, Client *unClient) {
	string choix; int choixInt;
	cout << "\nChoisissez votre transaction" << endl;
	cout << "\t 1 - Pour Retirer" << endl;
	cout << "\t 2 - Pour Deposer" << endl;
	cout << "\t 3 - Pour Consulter" << endl;
	cout << "\t 4 - Pour Quitter" << endl;

	do {
		cout << "Entrez votre choix <1-4> : ";
		getline(cin, choix);
		choixInt = atoi(choix.c_str()); //Converti le choix en int.

		switch (choixInt) {
		case 1:
			retirer(unClient);
			break;
		case 2:
			deposer(unClient);
			break;
		case 3:
			consulter(uneliste);
			break;
		case 4:
			cout << "\nMerci d'avoir utilise nous service !!!\n" << endl;
		}
	} while (choixInt >= 0 && choixInt < 4); //Return 0 si un string est entree. Donc la boucle recommence.
}

int main() {
	Liste *maliste = new Liste();
	Client *monClient = new Client();

	afficheTitre();
	recuperCompteDuFichier(maliste, monClient);
	choisirTransaction(maliste, monClient);
	system("pause");
	return 0;
}